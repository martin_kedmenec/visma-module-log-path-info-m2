<?php

namespace Unit1\LogPathInfo\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

/**
 * Class StorelistCommand
 */
class Log implements ObserverInterface
{
    /**
     * @var LoggerInterface $logger
     */
    private LoggerInterface $logger;

    /**
     * @var RequestInterface $request
     */
    private RequestInterface $request;

    /**
     * Log constructor
     *
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     */
    public function __construct(
        LoggerInterface $logger,
        RequestInterface $request
    ) {
        $this->logger = $logger;
        $this->request = $request;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $this->logger->critical('Request URI: ' . $this->request->getPathInfo());
    }
}
